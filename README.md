# Bonus Wheel
Bonus wheel game with animation.

## Notes:
1.) The solution file is inside the "proj.win32" folder.

2.) Unable to push cocos2d-x library files to Bitbucket due to file size. Your local cocos2d-x library files will be needed to run the solution file.

3.) Function for manually checking each sector of the wheel can be found in "HelloWorldScene.cpp" on line 241.

4.) Results for simulation of 1000 spins is output to console. Be sure to compile with debug and show output from "debug" to view results.