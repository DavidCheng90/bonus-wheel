/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

 /****************************************************************************
  Bonus Wheel by David Cheng 02/26/2020
 ****************************************************************************/
#define COCOS2D_DEBUG 1

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	pressedButton = false;

	//////////////////////////////
	// 2. get original background
	bgOriginal = Sprite::create("Circus.png");

	auto bgWidth = bgOriginal->getContentSize().width;
	auto bgHeight = bgOriginal->getContentSize().height;

	bgOriginal->setFlippedY(true);
	bgOriginal->setScale(visibleSize.width / bgWidth, visibleSize.height / bgHeight);
	bgOriginal->setPosition(visibleSize.width / 2, visibleSize.height / 2);

	//////////////////////////////
	// 3. create new texture with original background but use the exact size of the screen
	auto bgChangeSize = RenderTexture::create(visibleSize.width, visibleSize.height);

	bgChangeSize->begin();
	bgOriginal->visit();
	bgChangeSize->end();

	//////////////////////////////
	// 4. set the newly resized background
	bgResized = Sprite::createWithTexture(bgChangeSize->getSprite()->getTexture());

	bgResized->setPosition(Point((visibleSize.width / 2) + origin.x, (visibleSize.height / 2) + origin.y));
	this->addChild(bgResized);

	//////////////////////////////
	// 5. set wheel
	assetWheel = Sprite::create("wheel_sections_8.png");

	assetWheel->setPosition(visibleSize.width / 2, visibleSize.height / 1.7);
	assetWheel->setScale(0.28);
	this->addChild(assetWheel);

	// get position of wheel
	auto wheelPosition = assetWheel->getPosition();

	//////////////////////////////
	// 6. set wheel border
	assetWheelBorder = Sprite::create("wheel_border.png");

	assetWheelBorder->setPosition(wheelPosition);
	assetWheelBorder->setScale(0.28);
	this->addChild(assetWheelBorder);

	//////////////////////////////
	// 7. set wheel arrow
	assetArrow = Sprite::create("wheel_arrow.png");

	assetArrow->setPosition(wheelPosition.x, wheelPosition.y + 128);
	assetArrow->setScale(0.28);
	this->addChild(assetArrow);

	//////////////////////////////
	// 8. set spin button
	assetButton = ui::Button::create("spin_button.png");

	assetButton->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 6.5));
	assetButton->setScale(0.28);
	this->addChild(assetButton);

	// ***** event listener for spin button *****
	assetButton->addTouchEventListener(CC_CALLBACK_2(HelloWorld::pressButton, this));

	// spin button text
	spinText = ui::Text::create("Spin!", "Roboto-Black.ttf", 130);

	spinText->setPosition(Vec2(309, 137));
	spinText->setTextColor(Color4B::WHITE);
	spinText->enableOutline(Color4B(0, 210, 50, 255), 8);
	assetButton->addChild(spinText);

	//////////////////////////////
	// 9. set prizes

	// sector 1: "Life 30 min."
	assetHeart1 = Sprite::create("heart.png");                               // create sprite
	setProperties(assetHeart1, 645, 830, 1.15, 22.5, assetWheel);            // set properties (e.g. position, scale, rotation, etc.)
	assetHeart1Text = ui::Text::create("30", "Roboto-Black.ttf", 70 / 1.15); // create text
	addText(assetHeart1Text, assetHeart1);                                   // set properties for text (i.e. font color)
	assetHeart1SubText = ui::Text::create("min.", "Roboto-Black.ttf", 45);
	addSubText(assetHeart1SubText, assetHeart1Text);
	
	// sector 2: "Brush 3x"
	assetBrush2 = Sprite::create("brush.png");                               // create sprite
	setProperties(assetBrush2, 830, 650, 0.85, 67.5, assetWheel);            // set properties (e.g. position, scale, rotation, etc.)
	assetBrush2Text = ui::Text::create("x3", "Roboto-Black.ttf", 70 / 0.85); // create text
	addText(assetBrush2Text, assetBrush2);                                   // set properties for text (i.e. font color)

	// sector 3: "Gems 35"
	assetGem3 = Sprite::create("gem.png");
	setProperties(assetGem3, 830, 390, 1.15, 112.5, assetWheel);
	assetGem3Text = ui::Text::create("x35", "Roboto-Black.ttf", 70 / 1.15);
	addText(assetGem3Text, assetGem3);

	// sector 4: "Hammer 3x"
	assetHammer4 = Sprite::create("hammer.png");
	setProperties(assetHammer4, 645, 210, 1.05, 157.5, assetWheel);
	assetHammer4Text = ui::Text::create("x3", "Roboto-Black.ttf", 70 / 1.05);
	addText(assetHammer4Text, assetHammer4);

	// sector 5: "Coins 750"
	assetCoin5 = Sprite::create("coin.png");
	setProperties(assetCoin5, 390, 210, 0.95, 202.5, assetWheel);
	assetCoin5Text = ui::Text::create("x750", "Roboto-Black.ttf", 70 / 0.95);
	addText(assetCoin5Text, assetCoin5);

	// sector 6: "Brush 1x"
	assetBrush6 = Sprite::create("brush.png");
	setProperties(assetBrush6, 210, 390, 0.85, 247.5, assetWheel);
	assetBrush6Text = ui::Text::create("x1", "Roboto-Black.ttf", 70 / 0.85);
	addText(assetBrush6Text, assetBrush6);

	// sector 7: "Gems 75"
	assetGem7 = Sprite::create("gem.png");
	setProperties(assetGem7, 210, 650, 1.15, 292.5, assetWheel);
	assetGem7Text = ui::Text::create("x75", "Roboto-Black.ttf", 70 / 1.15);
	addText(assetGem7Text, assetGem7);

	// sector 8: Hammer 1x"
	assetHammer8 = Sprite::create("hammer.png");
	setProperties(assetHammer8, 390, 830, 1.05, 337.5, assetWheel);
	assetHammer8Text = ui::Text::create("x1", "Roboto-Black.ttf", 70 / 1.05);
	addText(assetHammer8Text, assetHammer8);

    return true;
}

void HelloWorld::pressButton(Ref *sender, ui::Widget::TouchEventType type)
{
	if (!pressedButton) // once user spins the wheel, user cannot spin again
	{
		switch (type)
		{
		case ui::Widget::TouchEventType::BEGAN: // user's finger is on the spin button, wind back the wheel a little bit
		{
			auto setWheel = RotateBy::create(1, -30);
			assetWheel->runAction(setWheel);
			break;
		}
		case ui::Widget::TouchEventType::ENDED: // user's finger let go of the spin button, spin the wheel
		{
			if (pressedButton = true) // once the wheel spins, fade out the spin button
			{
				auto buttonGone = FadeOut::create(1);
				assetButton->runAction(buttonGone);
				spinText->runAction(buttonGone->clone());
				playSound();
			}

			int randomNumber = 0;
			prizeCount[8] = { };
			result = 0.0f;
			prize = "0";

			for (int a = 0; a < 1000; ++a) // simulate 1000 spins and record results
			{
				randomNumber = cocos2d::RandomHelper::random_int(1, 20);

				collectPrize(a, randomNumber, 1, 4, prizeCount[0], result, 337.5, prize, "Life 30 min.");
				collectPrize(a, randomNumber, 5, 6, prizeCount[1], result, 292.5, prize, "Brush 3x");
				collectPrize(a, randomNumber, 7, 8, prizeCount[2], result, 247.5, prize, "Gems 35");
				collectPrize(a, randomNumber, 9, 10, prizeCount[3], result, 202.5, prize, "Hammer 3x");
				collectPrize(a, randomNumber, 11, 11, prizeCount[4], result, 157.5, prize, "Coins 750");
				collectPrize(a, randomNumber, 12, 15, prizeCount[5], result, 112.5, prize, "Brush 1x");
				collectPrize(a, randomNumber, 16, 16, prizeCount[6], result, 67.5, prize, "Gems 750");
				collectPrize(a, randomNumber, 17, 20, prizeCount[7], result, 22.5, prize, "Hammer 1x");
			}

			// ***** function for manually checking each sector of the wheel *****
			// how to use: Replace the "0" parameter with the number of the sector you wish to test
			// 1 - "Life 30 mins."
			// 2 - "Brush 3x"
			// 3 - "Gems 35"
			// 4 - Hammer 3x"
			// 5 - "Coins 750"
			// 6 - "Brush 1x"
			// 7 - "Gems 75"
			// 8 - "Hammer 1x"
			testWheelSector(0);
			// ********************************************************************

			// output to console the results of 1000 spins simulation
			// make sure the console shows output from "debug"
			CCLOG("\nWow! You've won \"%s\"!!!\n\nBonus Wheel 1000x Spins Results:\n1.) Life 30 mins.: %i\n2.) Brush 3x: %i\n3.) Gems 35: %i\n4.) Hammer 3x: %i\n5.) Coins 750: %i\n6.) Brush 1x: %i\n7.) Gems 75: %i\n8.) Hammer 1x: %i\n",
				prize.c_str(), prizeCount[0], prizeCount[1], prizeCount[2], prizeCount[3], prizeCount[4], prizeCount[5], prizeCount[6], prizeCount[7]);

			// spin wheel animation
			auto spinWheelStage1 = RotateTo::create(0.1, 0);
			auto spinWheelStage2 = Repeat::create(RotateBy::create(1, 360), 3);
			auto spinWheelStage3 = RotateBy::create(1.3, 360);
			auto spinWheelStage4 = RotateBy::create(1.7, 360);
			auto spinWheelStage5 = RotateBy::create((result / 22.5) * 0.1375, result);
			auto delayAnimation1 = DelayTime::create(1.0);
			CallFunc *playAnimation1 = CallFunc::create(std::bind(&HelloWorld::prizeAnimation1, this, yourPrize[0]));
			CallFunc *playAnimation2 = CallFunc::create(std::bind(&HelloWorld::prizeAnimation2, this, yourPrize[0]));
			CallFunc *showClaimButton = CallFunc::create(std::bind(&HelloWorld::claimButton, this));

			auto spinWheel = Sequence::create(spinWheelStage1, spinWheelStage2, spinWheelStage3, spinWheelStage4, spinWheelStage5,
				                              delayAnimation1, playAnimation1, playAnimation2, showClaimButton, NULL);

			assetWheel->runAction(spinWheel);

			break;
		}
		case ui::Widget::TouchEventType::CANCELED:
		{
			auto resetWheel = RotateTo::create(0, 0);
			assetWheel->runAction(resetWheel);
			break;
		}
		default:
			break;
		}
	}
}

// records results from simulating 1000 spins
void HelloWorld::collectPrize(int spinNum, int &randomNum, int numLowerBound, int numUpperBound, int &prizeCount,
	                          float &result, float rotateDegrees, std::string &prize, std::string prizeName)
{
	if (randomNum >= numLowerBound && randomNum <= numUpperBound)
	{
		prizeCount += 1;

		if (spinNum == 0) // use the first spin as the result for the wheel spin animation
		{
			result = rotateDegrees;
			prize = prizeName;

			if (prize == "Life 30 min.")
				yourPrize.push_back(assetHeart1);
			if (prize == "Brush 3x")
				yourPrize.push_back(assetBrush2);
			if (prize == "Gems 35")
				yourPrize.push_back(assetGem3);
			if (prize == "Hammer 3x")
				yourPrize.push_back(assetHammer4);
			if (prize == "Coins 750")
				yourPrize.push_back(assetCoin5);
			if (prize == "Brush 1x")
				yourPrize.push_back(assetBrush6);
			if (prize == "Gems 750")
				yourPrize.push_back(assetGem7);
			if (prize == "Hammer 1x")
				yourPrize.push_back(assetHammer8);
		}
	}
}

// fade out everything except for the prize won
void HelloWorld::prizeAnimation1(cocos2d::Sprite *yourPrize)
{
	auto wheelGone = FadeOut::create(1);

	assetWheel->runAction(wheelGone->clone());
	assetWheelBorder->runAction(wheelGone->clone());
	assetArrow->runAction(wheelGone->clone());

	if (yourPrize != assetHeart1)
	{
		assetHeart1->runAction(wheelGone->clone());
		assetHeart1Text->runAction(wheelGone->clone());
		assetHeart1SubText->runAction(wheelGone->clone());
	}
	if (yourPrize != assetBrush2)
	{
		assetBrush2->runAction(wheelGone->clone());
		assetBrush2Text->runAction(wheelGone->clone());
	}
	if (yourPrize != assetGem3)
	{
		assetGem3->runAction(wheelGone->clone());
		assetGem3Text->runAction(wheelGone->clone());
	}
	if (yourPrize != assetHammer4)
	{
		assetHammer4->runAction(wheelGone->clone());
		assetHammer4Text->runAction(wheelGone->clone());
	}
	if (yourPrize != assetCoin5)
	{
		assetCoin5->runAction(wheelGone->clone());
		assetCoin5Text->runAction(wheelGone->clone());
	}
	if (yourPrize != assetBrush6)
	{
		assetBrush6->runAction(wheelGone->clone());
		assetBrush6Text->runAction(wheelGone->clone());
	}
	if (yourPrize != assetGem7)
	{
		assetGem7->runAction(wheelGone->clone());
		assetGem7Text->runAction(wheelGone->clone());
	}
	if (yourPrize != assetHammer8)
	{
		assetHammer8->runAction(wheelGone->clone());
		assetHammer8Text->runAction(wheelGone->clone());
	}
}

// animation for prize won
void HelloWorld::prizeAnimation2(cocos2d::Sprite *yourPrize)
{
	if (yourPrize == assetHeart1) // animation for "Life 30 min."
	{
		auto jump1 = JumpTo::create(0.8, Point(775, 737), 125, 1);
		auto jump2 = JumpTo::create(0.8, Point(275, 630), 150, 1);
		auto jump3 = JumpTo::create(0.8, Point(514, 514), 150, 1);
		auto scale1 = ScaleTo::create(2, 3);

		auto animation = Spawn::create(Sequence::create(jump1, jump2, jump3, NULL), scale1, NULL);

		yourPrize->runAction(animation);
	}
	if (yourPrize == assetBrush2 || yourPrize == assetBrush6) // animation for "Brush 3x" and "Brush 1x"
	{
		auto rotate1 = RotateBy::create(0.5, -65);
		auto rotate2 = RotateBy::create(0.5, 65);
		auto move1 = MoveTo::create(2.5, Point(514, 514));
		auto scale1 = ScaleTo::create(2, 2);

		auto animation = Sequence::create(rotate1, rotate2, rotate1, rotate2, Spawn::create(move1, scale1, NULL), NULL);

		yourPrize->runAction(animation);
	}
	if (yourPrize == assetGem3 || yourPrize == assetGem7) // animation for "Gems 35" and "Gems 75"
	{
		ccBezierConfig bezier;
		bezier.controlPoint_1 = Point(1000, 1000);
		bezier.controlPoint_2 = Point(-1000, 1000);
		bezier.endPosition = Point(514, 514);
		auto bezier1 = BezierTo::create(2, bezier);
		auto scale1 = ScaleTo::create(2, 3);

		auto animation = Spawn::create(bezier1, scale1, NULL);

		yourPrize->runAction(animation);
	}
	if (yourPrize == assetHammer4 || yourPrize == assetHammer8) // animation for "Hammer 3x" and Hammer 1x"
	{
		auto rotate1 = RotateBy::create(0.2, -65);
		auto rotate2 = RotateBy::create(0.2, 65);
		DelayTime *delay1 = DelayTime::create(0.5);
		auto move1 = MoveTo::create(2, Point(514, 514));
		auto scale1 = ScaleTo::create(2, 2.5);

		auto animation = Sequence::create(rotate1, rotate2, rotate1, rotate2, rotate1, rotate2, delay1, Spawn::create(move1, scale1, NULL), NULL);

		yourPrize->runAction(animation);
	}
	if (yourPrize == assetCoin5) // animation for "Coins 750"
	{
		cocos2d::Sprite *Coin1 = Sprite::create("coin.png");
		setProperties(Coin1, 70, 70, 1, 0, assetCoin5);
		cocos2d::Sprite *Coin2 = Sprite::create("coin.png");
		setProperties(Coin2, 70, 70, 1, 0, assetCoin5);
		cocos2d::Sprite *Coin3 = Sprite::create("coin.png");
		setProperties(Coin3, 70, 70, 1, 0, assetCoin5);
		cocos2d::Sprite *Coin4 = Sprite::create("coin.png");
		setProperties(Coin4, 70, 70, 1, 0, assetCoin5);

		auto jump1 = JumpTo::create(1, Point(-200, 200), 150, 1);
		auto jump2 = JumpTo::create(1, Point(-200, -200), 150, 1);
		auto jump3 = JumpTo::create(1, Point(300, 200), 150, 1);
		auto jump4 = JumpTo::create(1, Point(300, -200), 150, 1);
		auto fade1 = FadeOut::create(1.1);
		auto move1 = MoveTo::create(2, Point(514, 514));
		auto scale1 = ScaleTo::create(1.5, 2.5);

		auto animation1 = Spawn::create(jump1, fade1->clone(), NULL);
		auto animation2 = Spawn::create(jump2, fade1->clone(), NULL);
		auto animation3 = Spawn::create(jump3, fade1->clone(), NULL);
		auto animation4 = Spawn::create(jump4, fade1->clone(), NULL);
		auto animation5 = Spawn::create(move1, scale1, NULL);

		Coin1->runAction(animation1);
		Coin2->runAction(animation2);
		Coin3->runAction(animation3);
		Coin4->runAction(animation4);
		yourPrize->runAction(animation5);
	}
}

// show claim button after user wins prize
void HelloWorld::claimButton()
{
	ui::Text *claimText = ui::Text::create("Claim!", "Roboto-Black.ttf", 130);

	claimText->setPosition(Vec2(309, 120));
	claimText->setTextColor(Color4B::WHITE);
	claimText->enableOutline(Color4B(0, 210, 50, 255), 8);
	assetButton->addChild(claimText);

	auto hideButton = FadeOut::create(0);
	auto buttonAppear = FadeIn::create(0.5);
	auto showText = Sequence::create(hideButton, buttonAppear->clone(), NULL);

	assetButton->runAction(buttonAppear->clone());
	claimText->runAction(showText);
}

void HelloWorld::playSound()
{
	auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->playEffect("audio/Sound.wav", false, 1.0, 1.0, 1.0);
}

void HelloWorld::setProperties(cocos2d::Sprite *sprite, int posX, int posY, double scaleSize, double rotateDegrees, cocos2d::Sprite *parent)
{
	sprite->setPosition(posX, posY);
	sprite->setScale(scaleSize);
	sprite->setRotation(rotateDegrees);
	parent->addChild(sprite);
}

void HelloWorld::addText(ui::Text *text, cocos2d::Sprite *parent)
{
	text->setPosition(Vec2(parent->getContentSize().width - text->getContentSize().width / 2, text->getContentSize().height / 2));
	text->setTextColor(Color4B::WHITE);
	text->enableOutline(Color4B(141, 94, 47, 255), 4);
	parent->addChild(text);
}

void HelloWorld::addSubText(ui::Text *text, ui::Text *parent)
{
	text->setPosition(Vec2(parent->getContentSize().width - 10, 0));
	text->setTextColor(Color4B::WHITE);
	text->enableOutline(Color4B(141, 94, 47, 255), 4);
	parent->addChild(text);
}

// ***** function for manually checking each sector of the wheel *****
void HelloWorld::testWheelSector(int sectorNum)
{
	if (sectorNum >= 1 && sectorNum <= 8)
	{
		yourPrize.pop_back();

		if (prize == "Life 30 min.")
			prizeCount[0] -= 1;
		if (prize == "Brush 3x")
			prizeCount[1] -= 1;
		if (prize == "Gems 35")
			prizeCount[2] -= 1;
		if (prize == "Hammer 3x")
			prizeCount[3] -= 1;
		if (prize == "Coins 750")
			prizeCount[4] -= 1;
		if (prize == "Brush 1x")
			prizeCount[5] -= 1;
		if (prize == "Gems 750")
			prizeCount[6] -= 1;
		if (prize == "Hammer 1x")
			prizeCount[7] -= 1;
	}

	switch (sectorNum)
	{
	case 1:
		result = 337.5;
		prize = "Life 30 min.";
		yourPrize.push_back(assetHeart1);
		prizeCount[0] += 1;
		break;
	case 2:
		result = 292.5;
		prize = "Brush 3x";
		yourPrize.push_back(assetBrush2);
		prizeCount[1] += 1;
		break;
	case 3:
		result = 247.5;
		prize = "Gems 35";
		yourPrize.push_back(assetGem3);
		prizeCount[2] += 1;
		break;
	case 4:
		result = 202.5;
		prize = "Hammer 3x";
		yourPrize.push_back(assetHammer4);
		prizeCount[3] += 1;
		break;
	case 5:
		result = 157.5;
		prize = "Coins 750";
		yourPrize.push_back(assetCoin5);
		prizeCount[4] += 1;
		break;
	case 6:
		result = 112.5;
		prize = "Brush 1x";
		yourPrize.push_back(assetBrush6);
		prizeCount[5] += 1;
		break;
	case 7:
		result = 67.5;
		prize = "Gems 750";
		yourPrize.push_back(assetGem7);
		prizeCount[6] += 1;
		break;
	case 8:
		result = 22.5;
		prize = "Hammer 1x";
		yourPrize.push_back(assetHammer8);
		prizeCount[7] += 1;
		break;
	default:
		break;
	}
}
