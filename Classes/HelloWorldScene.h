/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

USING_NS_CC_EXT;
//using namespace ui;

class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	// original background
	cocos2d::Sprite *bgOriginal;

	// resized background
	cocos2d::Sprite *bgResized;

	// asset sprites & texts
	cocos2d::Sprite *assetWheel;
	cocos2d::Sprite *assetWheelBorder;
	cocos2d::Sprite *assetArrow;

	ui::Button *assetButton;
	ui::Text *spinText;

	cocos2d::Sprite *assetHeart1;
	cocos2d::Sprite *assetBrush2;
	cocos2d::Sprite *assetGem3;
	cocos2d::Sprite *assetHammer4;
	cocos2d::Sprite *assetCoin5;
	cocos2d::Sprite *assetBrush6;
	cocos2d::Sprite *assetGem7;
	cocos2d::Sprite *assetHammer8;

	ui::Text *assetHeart1Text;
	ui::Text *assetHeart1SubText;
	ui::Text *assetBrush2Text;
	ui::Text *assetGem3Text;
	ui::Text *assetHammer4Text;
	ui::Text *assetCoin5Text;
	ui::Text *assetBrush6Text;
	ui::Text *assetGem7Text;
	ui::Text *assetHammer8Text;

	std::vector<cocos2d::Sprite*> yourPrize;

	// methods
	void pressButton(Ref *sender, ui::Widget::TouchEventType type);
	void collectPrize(int spinNum, int &randomNum, int numLowerBound, int numUpperBound, int &prizeCount, float &result, float rotateDegrees, std::string &prize, std::string prizeName);
	void prizeAnimation1(cocos2d::Sprite *yourPrize);
	void prizeAnimation2(cocos2d::Sprite *yourPrize);
	void claimButton();

	void playSound();

	void setProperties(cocos2d::Sprite *sprite, int posX, int posY, double scaleSize, double rotateDegrees, cocos2d::Sprite *parent);
	void addText(ui::Text *text, cocos2d::Sprite *parent);
	void addSubText(ui::Text *text, ui::Text *parent);

	// ***** function for manually checking each sector of the wheel *****
	void testWheelSector(int sectorNum);

private:
	bool pressedButton;
	int prizeCount[8];
	float result;
	std::string prize;
};

#endif // __HELLOWORLD_SCENE_H__
